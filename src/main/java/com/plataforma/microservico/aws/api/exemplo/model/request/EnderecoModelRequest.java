package com.plataforma.microservico.aws.api.exemplo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class EnderecoModelRequest {

    @JsonProperty("id_endereco")
    private String idEndereco;

    @JsonProperty("rua")
    private String rua;

    @JsonProperty("numero")
    private Integer numero;
    
    @JsonProperty("bairro")
    private String bairro;

    @JsonProperty("cidade")
    private String cidade;

    @JsonProperty("uf")
    private String estado;

    @JsonProperty("complemento")
    private String complemento;

    @JsonProperty("cep")
    private String cep;
    
}
