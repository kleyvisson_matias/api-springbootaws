package com.plataforma.microservico.aws.api.exemplo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class InstituicaoEnsinoModelRequest {
    
    @JsonProperty("id_instituicao")
    private String idInstituicao;

    @JsonProperty("nome")
    private String nome;

    @JsonProperty("sigla")
    private String sigla;

    @JsonProperty("tel_contato")
    private String contato;
    
    @JsonProperty("denominacao_ies")
    private String denominacaoIES;

    @JsonProperty("endereco")
    private EnderecoModelRequest endereco;
    
}
