package com.plataforma.microservico.aws.api.exemplo.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class AlunoModelRequest {

    @JsonProperty("id_aluno")
    private String idAluno;

    @JsonProperty("nome")
    private String nome;
    
    @JsonProperty("data_nascimento")
    @JsonFormat(pattern="yyyy-MM-dd")
    private String dataNascimento;

    @JsonProperty("idade")
    private Integer idade;

    @JsonProperty("sexo")
    private String sexo;

    @JsonProperty("cpf")
    private String cpf;

    @JsonProperty("matricula")
    private String matricula;
    
    @JsonProperty("tel_contato")
    private String contato;

    @JsonProperty("email")
    private String email;

    @JsonProperty("curso")
    private String curso;

    @JsonProperty("endereco")
    private EnderecoModelRequest endereco;
}
