package com.plataforma.microservico.aws.api.exemplo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/instituicao")
public class InstituicaoEnsinoController {
    
    @GetMapping
	public String instituicao() {
		return "Instituicao";
	}
}
