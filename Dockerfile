FROM openjdk:11-jre
RUN mkdir app
ARG JAR_FILE
ADD /target/${JAR_FILE} /app/spring-exemplo-api-aws.jar
WORKDIR /app
EXPOSE 8090
ENTRYPOINT java -jar spring-exemplo-api-aws.jar